package net.qqxh.persistent;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import net.qqxh.common.utils.FileAnalysisTool;
import net.qqxh.persistent.redis.PropertiesUtils;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

/**
 *
 * @author jason
 */
@Configuration

public class JfSysUserData {

    @Autowired
    PropertiesUtils propertiesUtils;
    /**
     * 存用户名和密码
     */
    private static Map<String, JfUserSimple> userMap;
    private static Map<String, String> permissions;
    private  static Map<String ,SearchLib> searchLibMap;
    @PostConstruct
    public  void setRealmDB() throws FileNotFoundException {
        String  realmdbFileName=   propertiesUtils.getPropertiesValue("realmdbFileName");
       File file= FileAnalysisTool.getResFile(realmdbFileName);
        userMap=new HashMap<>();
        permissions=new HashMap<>();
        searchLibMap=new HashMap<>();
        String realmDBStr = null;
        try {
            realmDBStr=FileUtils.readFileToString(file,Charset.forName("UTF-8"));
            JSONObject jsonObject = JSONObject.parseObject(realmDBStr);
            JSONArray users = (JSONArray) jsonObject.get("users");
            JSONArray searchLibs = (JSONArray) jsonObject.get("searchLibs");
            JSONArray permissionsData = (JSONArray) jsonObject.get("permissions");

            for (Object o : searchLibs) {
                JSONObject j = (JSONObject) o;
                SearchLib searchLib = j.toJavaObject(SearchLib.class);
                searchLibMap.put(searchLib.getLibId(),searchLib);
            }
            for (Object o : users) {
                JSONObject j = (JSONObject) o;
                JfUserSimple jfUserSimple = j.toJavaObject(JfUserSimple.class);
                jfUserSimple.setSearchLib(searchLibMap.get(jfUserSimple.getSearchLibId()));
                userMap.put(jfUserSimple.getUserid(), jfUserSimple);
            }
            for (Object o : permissionsData) {
                JSONObject j = (JSONObject) o;
                permissions.put(j.getString("url"), j.getString("permission"));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Map<String, JfUserSimple> getUserMap() {
        return userMap;
    }

    public   Map<String, String> getPermissions() {
        return permissions;
    }

    private static Map mapConstructor(String key, String value) {
        Map userrole = new HashMap();
        userrole.put(key, value);
        return userrole;
    }
    public static String getPasswordByUserName(String username) {
        if(userMap.get(username)==null){
            return null;
        }
        return userMap.get(username).getPwd();
    }
    public static JfUserSimple getUserByUserId(String username) {
        return userMap.get(username);
    }
    public static Set<String> getPermissionsByUserName(String username) {
        List<String> list = userMap.get(username).getRoles();
        Set roles = new HashSet<>();
        for (String s : list) {
            roles.add(s);
        }
        return roles;
    }

    public static Map<String, SearchLib> getSearchLibMap() {
        return searchLibMap;
    }
}
