package net.qqxh.controller.common;

public class CommonJson {
    /**
     * 请求状态
     */
    private String status;

    /**
     * 短标题
     */
    private String msg;

    /**
     * 用于描述详细信息：
     */
    private String detail;

    /**
     * 业务数据
     */
    private Object data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Object getData() {
        return this.data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
